package com.netappsid.timesheet;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Holiday
{
	private final String description;
	private final BigDecimal duration;
	private final LocalDate date;

	private Holiday(Builder builder)
	{
		this.description = builder.description;
		this.duration = builder.duration;
		this.date = builder.date;
	}

	public String getDescription()
	{
		return description;
	}

	public BigDecimal getDuration()
	{
		return duration;
	}

	public LocalDate getDate()
	{
		return date;
	}

	public static class Builder
	{
		private String description;
		private BigDecimal duration;
		private LocalDate date;

		public Builder description(String description)
		{
			this.description = description;
			return this;
		}

		public Builder duration(BigDecimal duration)
		{
			this.duration = duration;
			return this;
		}

		public Builder date(LocalDate date)
		{
			this.date = date;
			return this;
		}

		public Holiday build()
		{
			return new Holiday(this);
		}
	}
}
