package com.netappsid.timesheet;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Iterator;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netappsid.timesheet.config.Config;
import com.netappsid.timesheet.task.DynamicTimeTask;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TimesheetExtractor
{
	private static final String CLI_DISPLAY = "d";
	private static final String CLI_TOTAL = "t";
	private static final String CLI_START = "s";
	private static final String CLI_END = "e";
	private static final String CLI_USERNAME = "u";
	private static final String CLI_PASSWORD = "p";

	private static final String DURATION_SECONDS = "durationSeconds";
	private static final String KEY = "key";
	private static final String ISSUE = "issue";
	private static final String TIME_SPENT = "timeSpentSeconds";
	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	private static final String DAYS = "days";
	private static final String TYPE = "type";
	private static final String NAME = "name";
	private static final String HOLIDAY_TYPE = "HOLIDAY";
	private static final String HOLIDAY = "holiday";
	private static final String HOLIDAY_AND_NON_WORKING_DAY_TYPE = "HOLIDAY_AND_NON_WORKING_DAY";
	private static final String APPLICATION_JSON = "application/json; charset=utf-8";
	private static final int HOUR_PER_DAY = 8;
	private static final int REAL_HOUR_PER_DAY = 24;
	private static final int SECOND_PER_HOUR = 3600;
	private static final int HOUR_PER_WEEK = 40;

	private String cookie;
	private boolean display;
	private String username;
	private String password;
	private LocalDate startDate;
	private LocalDate endDate;
	private BigDecimal total;

	public static void main(String[] args)
	{
		Options options = new Options();
		options.addOption(Option.builder().desc("Display").longOpt(CLI_DISPLAY).build());
		options.addOption(Option.builder(CLI_TOTAL).desc("Total").longOpt("total").hasArg().build());
		options.addOption(Option.builder(CLI_START).desc("Start").longOpt("start").hasArg().build());
		options.addOption(Option.builder(CLI_END).desc("End").longOpt("end").hasArg().build());
		options.addOption(Option.builder(CLI_USERNAME).required().desc("Username").longOpt("user").hasArg().build());
		options.addOption(Option.builder(CLI_PASSWORD).required().desc("Password").longOpt("password").hasArg().build());

		CommandLineParser parser = new DefaultParser();
		Builder extractorBuilder = TimesheetExtractor.builder();

		try
		{
			CommandLine commandLine = parser.parse(options, args);

			if (commandLine.hasOption(CLI_DISPLAY))
			{
				extractorBuilder.display(true);
			}

			if (commandLine.hasOption(CLI_TOTAL))
			{
				extractorBuilder.total(BigDecimal.valueOf(SECOND_PER_HOUR * Double.parseDouble(commandLine.getOptionValue(CLI_TOTAL))));
			}

			if (commandLine.hasOption(CLI_START))
			{
				extractorBuilder.startDate(LocalDate.parse(commandLine.getOptionValue(CLI_START)));
			}

			if (commandLine.hasOption(CLI_END))
			{
				extractorBuilder.endDate(LocalDate.parse(commandLine.getOptionValue(CLI_END)));

			}

			extractorBuilder.username(commandLine.getOptionValue(CLI_USERNAME));
			extractorBuilder.password(commandLine.getOptionValue(CLI_PASSWORD));
		}
		catch (ParseException e)
		{
			System.out.println(e.getMessage());
		}

		TimesheetExtractor extractor = extractorBuilder.build();
		extractor.execute();
	}

	private TimesheetExtractor(Builder builder)
	{
		this.username = builder.username;
		this.password = builder.password;
		this.display = builder.display;
		this.startDate = builder.startDate;
		this.endDate = builder.endDate;
		this.total = builder.total;
	}

	public void execute()
	{
		LocalDate from = this.startDate;
		LocalDate to = from.plusDays(6);
		BigDecimal total = this.total;
		LocalDate maxDate = this.endDate;

		while (from.compareTo(maxDate) < 0)
		{
			BigDecimal deltaPerWeek = getDeltaPerWeek(from, to);

			if (display)
			{
				System.out.println(from + " -> " + to + ": " + deltaPerWeek.divide(BigDecimal.valueOf(SECOND_PER_HOUR), 2, RoundingMode.HALF_UP));
			}

			total = total.add(deltaPerWeek);

			from = to.plusDays(1);
			to = from.plusDays(6);
		}

		System.out.println("total (hours): " + total.divide(BigDecimal.valueOf(SECOND_PER_HOUR), 2, RoundingMode.HALF_UP));
	}

	private String getHolidays(LocalDate from, LocalDate to)
	{
		return getResponse(Config.HOLIDAY_URL.getUrl(from.toString(), to.toString()));
	}

	private String getWokLogs(LocalDate from, LocalDate to)
	{
		return getResponse(Config.WORKLOGS_URL.getUrl(from.toString(), to.toString()));
	}

	private BigDecimal getDeltaPerWeek(LocalDate from, LocalDate to)
	{
		String workLogs = getWokLogs(from, to);
		ObjectMapper mapper = new ObjectMapper();
		BigDecimal total = getHolidayDurationPerWeek(from, to);
		BigDecimal negativeBank = BigDecimal.ZERO;
		BigDecimal totalPerWeek = BigDecimal.valueOf(HOUR_PER_WEEK * SECOND_PER_HOUR);

		try
		{
			JsonNode workLogsNode = mapper.readTree(workLogs);
			Iterator<JsonNode> iterator = workLogsNode.iterator();

			while (iterator.hasNext())
			{
				JsonNode worklog = iterator.next();

				BigDecimal timeSpent = BigDecimal.valueOf(worklog.get(TIME_SPENT).doubleValue());
				String issueId = worklog.get(ISSUE).get(KEY).textValue();
				BigDecimal realTimeSpent = DynamicTimeTask.getRealTimeSpent(issueId, timeSpent);

				// if the real time spent is negative, so we use the bank
				if (realTimeSpent.compareTo(BigDecimal.ZERO) < 0)
				{
					realTimeSpent = realTimeSpent.multiply(BigDecimal.valueOf(-1));
					negativeBank = negativeBank.add(realTimeSpent);
				}

				total = total.add(realTimeSpent);
			}

			// if the total (including the holidays + bank) < 40h, then the difference must be taken from bank. In the other case, if the total > 40h, then the
			// difference must me place in the bank.
			negativeBank = total.subtract(totalPerWeek).subtract(negativeBank);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}

		return negativeBank;
	}

	private BigDecimal getHolidayDurationPerWeek(LocalDate from, LocalDate to)
	{
		String holidays = getHolidays(from, to);
		ObjectMapper mapper = new ObjectMapper();
		BigDecimal total = BigDecimal.ZERO;

		try
		{
			JsonNode workLogsNode = mapper.readTree(holidays);
			JsonNode days = workLogsNode.get(DAYS);
			Iterator<JsonNode> iterator = days.iterator();

			while (iterator.hasNext())
			{
				JsonNode day = iterator.next();

				if (day.get(TYPE).textValue().equalsIgnoreCase(HOLIDAY_TYPE) || day.get(TYPE).textValue().equalsIgnoreCase(HOLIDAY_AND_NON_WORKING_DAY_TYPE))
				{
					double duration = day.get(HOLIDAY).get(DURATION_SECONDS).asDouble();
					// Because the duration of 1 DAY in JIRA is 24h instead of 8, we need to use 28800 sec instead of 86400 sec. So we devide by 24 and multiply
					// by 8.
					total = total.add(BigDecimal.valueOf(duration).divide(BigDecimal.valueOf(REAL_HOUR_PER_DAY)).multiply(BigDecimal.valueOf(HOUR_PER_DAY)));
				}
			}
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}

		return total;
	}

	private String getCookie()
	{
		if (cookie == null)
		{
			OkHttpClient client = new OkHttpClient.Builder().build();
			String url = Config.JIRA_BASE_URL.getUrl() + Config.AUTH_URL.getUrl();
			ObjectMapper mapper = new ObjectMapper();

			ObjectNode authObject = mapper.createObjectNode();
			authObject.put("username", username);
			authObject.put("password", password);

			RequestBody requestBody = RequestBody.create(MediaType.parse(APPLICATION_JSON), authObject.toString());
			Request request = new Request.Builder().url(url).post(requestBody).build();

			try
			{
				Response response = client.newCall(request).execute();
				JsonNode responseJson = mapper.readTree(response.body().string());

				JsonNode session = responseJson.get("session");

				if (session == null)
				{
					throw new IllegalStateException("Invalid username/password");
				}

				cookie = session.get(NAME).textValue() + "=" + session.get("value").textValue();
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}
		}

		return cookie;
	}

	private String getResponse(String relativeUrl)
	{
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().addHeader("cookie", getCookie()).url(Config.JIRA_BASE_URL.getUrl() + relativeUrl).get().build();

		try
		{
			Response response = client.newCall(request).execute();
			return response.body().string();
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	public static Builder builder()
	{
		return new TimesheetExtractor.Builder();
	}

	public static class Builder
	{
		private boolean display;
		private String username;
		private String password;
		private LocalDate startDate;
		private LocalDate endDate;
		private BigDecimal total = BigDecimal.ZERO;

		public Builder()
		{
			// first week in JIRA
			startDate = LocalDate.of(2015, 9, 6);

			// Get previous Sunday if we are in the middle of a week
			endDate = LocalDate.now().getDayOfWeek() == DayOfWeek.SUNDAY ? LocalDate.now() : LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
		}

		public Builder display(boolean display)
		{
			this.display = display;
			return this;
		}

		public Builder username(String username)
		{
			this.username = username;
			return this;
		}

		public Builder password(String password)
		{
			this.password = password;
			return this;
		}

		public Builder startDate(LocalDate startDate)
		{
			this.startDate = startDate;
			return this;
		}

		public Builder endDate(LocalDate endDate)
		{
			this.endDate = endDate;
			return this;
		}

		public Builder total(BigDecimal total)
		{
			this.total = total;
			return this;
		}

		public TimesheetExtractor build()
		{
			return new TimesheetExtractor(this);
		}
	}
}
