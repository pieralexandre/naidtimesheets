package com.netappsid.timesheet.config;

public enum Config
{
	JIRA_BASE_URL("https://jira.360-innovations.com"), HOLIDAY_URL("/rest/tempo-core/1/user/schedule/?from=%s&to=%s"), WORKLOGS_URL(
			"/rest/tempo-timesheets/3/worklogs/?dateFrom=%s&dateTo=%s"), AUTH_URL("/rest/auth/1/session")

	;

	private final String url;

	Config(String url)
	{
		this.url = url;
	}

	public String getUrl(String... parameter)
	{
		return String.format(url, parameter);
	}
}
