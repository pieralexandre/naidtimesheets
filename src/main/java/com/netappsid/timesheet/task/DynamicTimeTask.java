package com.netappsid.timesheet.task;

import java.math.BigDecimal;

public enum DynamicTimeTask
{
	TASK1("TASK-1", 0), TASK3("TASK-3", -1), TASK8("TASK-8", -1), TASK10("TASK-10", 1), TASK11("TASK-11", 0.5);

	private double factor;
	private String name;

	DynamicTimeTask(String name, double factor)
	{
		this.factor = factor;
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public double getFactor()
	{
		return factor;
	}

	public BigDecimal getRealTimeSpent(BigDecimal timeSpent)
	{
		return BigDecimal.valueOf(factor).multiply(timeSpent);
	}

	public static BigDecimal getRealTimeSpent(String issueId, BigDecimal timeSpent)
	{
		for (DynamicTimeTask timeTask : DynamicTimeTask.values())
		{
			if (timeTask.getName().equalsIgnoreCase(issueId))
			{
				return timeTask.getRealTimeSpent(timeSpent);
			}
		}

		return timeSpent;
	}
}
